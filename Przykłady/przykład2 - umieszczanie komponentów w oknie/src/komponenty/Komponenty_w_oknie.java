package komponenty;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

public class Komponenty_w_oknie extends JFrame {

    public Komponenty_w_oknie(){

        JPanel panel = new JPanel();  //tworzymy panel, na którym będą komponenty


        JFrame okno = new JFrame ("Komponenty_w_oknie");  //tworzymy okno
        okno.add(panel);    //panel wraz z komponentami dodajemy do okna
        okno.setSize(400,300);  //rozmiar okna
        okno.setLocation(200,200);  //lokalizacja



        okno.setResizable(false);   //blokada zmiany rozmiaru ekranu
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    //operacja zamknięcia okna
        okno.setVisible(true);  //widoczność okna na ekranie


    }

}
