package zarzadcaRozkladu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ZarzadcaRozkladu extends JFrame implements ActionListener {

    private JButton przycisk;      //deklarujemy potrzebne komponenty czyli
    private JButton przycisk2;     //przyciski, etykiety, pola tekstowe
    private JLabel etykieta1;      //oraz kontenery czyli panel i okno
    private JLabel etykieta2;
    private JTextField poleTekstowe;
    private JTextField poleTekstowe2;
    private JPanel panel;
    private JFrame okno;

    public ZarzadcaRozkladu() {

        okno = new JFrame("Zarządca rozkładu");
        okno.setSize(400, 200);
        okno.setLocation(400,450);
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        przycisk = new JButton("czerwony");
        przycisk2 = new JButton("zielony");
        etykieta1 = new JLabel("Etykieta 1");
        etykieta2 = new JLabel("Etykieta 2");
        poleTekstowe = new JTextField("Pole tekstowe 1");
        poleTekstowe2 = new JTextField("Pole tekstowe 2");

        przycisk.addActionListener(this);     //dodajemy do przycisków słuchaczy
        przycisk2.addActionListener(this);

        panel.setLayout(new GridLayout(2, 3, 15, 15));
        //dodanie zarządcy rozkładu w postaci szachownicy 2x3

        panel.add(etykieta1);
        panel.add(poleTekstowe);
        panel.add(przycisk);
        panel.add(etykieta2);
        panel.add(poleTekstowe2);
        panel.add(przycisk2);

        okno.add(panel);
        okno.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source==przycisk)
        {
            panel.setBackground(Color.RED);
            poleTekstowe2.setText("Pole tekstowe 2");
        }
        else if(source==przycisk2)
        {
            panel.setBackground(Color.GREEN);
            poleTekstowe2.setText("test");
        }
    }
}
