package okno_puste;

import javax.swing.*;

public class Okno extends JFrame {

    public Okno()
    {
     super("Pusta ramka");//tytuł okna

     setSize(400,100); //ustawiamy rozmiar okna
     setLocation(500,400);//ustawiamy lokalizacje stworzonego okna

     setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//operacja zamknięcia okna

     setResizable(false); //zablokowanie dynamiecznej zmiany rozmiaru okna
     setVisible(true);//wyświetlanie okna na ekranie
    }

}
