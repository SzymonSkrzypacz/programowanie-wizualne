package calculator;

import org.mariuszgromada.math.mxparser.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Calculator extends JFrame implements ActionListener, KeyListener {

private JTextField textField = new JTextField();


    private ArrayList<JButton> controls;

    String[] keyValues = {
            "7", "8", "9", "*", "C","CE",
            "4", "5", "6", "/", "^","sqrt()",
            "1", "2", "3", "+", "(",")",
            "0", ".", "=", "-", "!","log"
    };
    Font font = new Font("Arial", Font.BOLD, 28);
    Font fontForLabel = new Font("Arial", Font.ITALIC, 34);

    private JPanel btnPanel = new JPanel(new GridLayout(4,6,4,4));


    public void createGUI()
    {
        controls = new ArrayList<JButton>();
        for (String keyValue : keyValues) {

            JButton b = new JButton(keyValue);
            b.addActionListener(this);
            b.addKeyListener(this);
            b.setFont(font);
            controls.add(b);
            btnPanel.add(b);
        }


        JMenuBar menubar = new JMenuBar();
        JMenu menufile = new JMenu("File");
        JMenuItem newfile = new JMenuItem(new AbstractAction("New") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                new Calculator();
            }
        });

        JMenuItem close = new JMenuItem(new AbstractAction("Close") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JMenu colors = new JMenu("Colors");
        JMenuItem changebuttonsbackground = new JMenuItem(new AbstractAction("Change color of buttons"){
        @Override
        public void actionPerformed(ActionEvent e){
            Color c = JColorChooser.showDialog(null, "Choose a color", Color.WHITE);
            for (int i = 0; i < controls.size(); i++) {
                JButton b =controls.get(i);
                b.setBackground(c);

            }
        }
    });

        JMenuItem changebackground = new JMenuItem(new AbstractAction("Change color of background"){
            @Override
            public void actionPerformed(ActionEvent e){
                Color c = JColorChooser.showDialog(null, "Choose a color", Color.WHITE);
                btnPanel.setBackground(c);

            }
        });

        JMenuItem changebuttonsforeground = new JMenuItem(new AbstractAction("Change color of font"){
            @Override
            public void actionPerformed(ActionEvent e){
                Color c = JColorChooser.showDialog(null, "Choose a color", Color.WHITE);
                for (int i = 0; i < controls.size(); i++) {
                    JButton b =controls.get(i);
                    b.setForeground(c);
                }

                textField.setForeground(c);

            }
        });

        JMenu about= new JMenu("Help");
        JMenuItem help = new JMenuItem(new AbstractAction("About converter"){
            @Override
            public void actionPerformed(ActionEvent ev) {
                JOptionPane.showMessageDialog(
                        Calculator.this, "This is a calculator that can make advanced operations. "
                        , "Help",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });

        menufile.add(newfile);
        menufile.add(close);
        menubar.add(menufile);
        colors.add(changebuttonsbackground);
        colors.add(changebackground);
        colors.add(changebuttonsforeground);
        menubar.add(colors);
        about.add(help);
        menubar.add(about);
        setJMenuBar(menubar);
    }


    Calculator()

    {
        super("Calculator");
        createGUI();
        setLayout(new GridLayout(2,1));
        textField.setFont(fontForLabel);
        textField.setHorizontalAlignment(SwingConstants.RIGHT);
        add(textField);
        add(btnPanel);

        setSize(670,455);
        setLocation(200,250);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

    }



    @Override
    public void actionPerformed(ActionEvent e) {

        Object o = e.getSource();
        if (o==controls.get(20))
        {
            Expression exp = new Expression(textField.getText());
            String temp2 = Double.toString(exp.calculate());
            textField.setText(temp2);
        }
        else if (o==controls.get(4)) {
            textField.setText(""+textField.getText().substring(0, textField.getText().length() - 1));
        } else if (o==controls.get(5)) {
            textField.setText("");
        } else {
            JButton b = null;
            String buttonText = "";

            if(o instanceof JButton)
                b = (JButton)o;

            if(b != null)
                buttonText = b.getText();

            textField.setText(textField.getText()+buttonText);
        }
    }



    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
